from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi
import json
import urllib.parse
import os
USERNAME = os.environ["MONGODB_CLOUD_USERNAME"]
PASSWD = os.environ["MONGODB_CLOUD_PASSWORD"]
DB = os.environ["MONGODB_CLOUD_DB"]
COLLECTION = os.environ["MONGODB_CLOUD_COLLECTION"]
DATA_PATH="/output"


def insert_json_file(json_file_path):
    # Load JSON data from file
    with open(json_file_path, "r") as json_file:
        json_data = json.load(json_file)
    
    # Escape the username and password using quote_plus
    username = urllib.parse.quote_plus(USERNAME)
    password = urllib.parse.quote_plus(PASSWD)
    
    # This a specific connection to the current free MongoDB instance
    uri = f"mongodb+srv://{username}:{password}@cluster0.t3vfep0.mongodb.net/?retryWrites=true&w=majority"
 
    # Connect to MongoDB
    client = MongoClient(uri, server_api=ServerApi('1'))
    db = client[DB]
    collection = db[COLLECTION]

    if client:
        print(f"Connected to server: {uri}")


    # Insert the JSON data into the collection
    query = {"github_id": json_data["github_id"]}
    existing_document = collection.find_one(query)

    if existing_document:
        # Update the existing document
        collection.update_one(query, {"$set": json_data})
        print("Document updated successfully with ID:", existing_document["_id"])
        
    else:
        # Insert the new document
        insert_result = collection.insert_one(json_data)
        # Check if the insertion was successful
        if insert_result.inserted_id:
            print("Document inserted successfully with ID:", insert_result.inserted_id)
        else:
            print("Failed to insert document")

    # Close the MongoDB connection    
    client.close()




def main():
    for filename in os.listdir(DATA_PATH):
        if filename.endswith(".json"):
            print(f"Processing file: {filename}")
            file_path = os.path.join(DATA_PATH,filename)
            insert_json_file(file_path)
            

if __name__ == "__main__":
    main()