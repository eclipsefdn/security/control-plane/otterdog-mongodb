import pymongo
import json
import urllib.parse
import sys
import os
USERNAME = "otterdog"
PASSWD = "otterdog"


# Replace these with your actual MongoDB connection details
db_name = "otterdogdb"
collection_name = "organizations"
data_path="/output"



def insert_json_file(json_file_path):
    # Load JSON data from file
    print(f"Processing file {json_file_path}")
    with open(json_file_path, "r") as json_file:
        json_data = json.load(json_file)

    if json_data:
        print(f'Data loaded from file: {json_file_path} for {json_data["github_id"]}')    
   # Escape the username and password using quote_plus
    username = urllib.parse.quote_plus(USERNAME)
    password = urllib.parse.quote_plus(PASSWD)

    uri = f"mongodb://{username}:{password}@otterdog_db:27017/"
 
    # Connect to MongoDB
    client = pymongo.MongoClient('otterdog_db', username=USERNAME, password=PASSWD, authSource=db_name,authMechanism='SCRAM-SHA-256')
    db = client[db_name]
    collection = db[collection_name]

    if client:
        print(f"Connected to server: {uri}")




    
    # Update the existing document
    # Insert the JSON data into the collection
    query = {"github_id": json_data["github_id"]}
    existing_document = collection.find_one(query)
    if existing_document:
        collection.update_one(query, {"$set": json_data})
        print("Document updated successfully with ID:", existing_document["_id"])
    else:
        # Insert the new document
        print(f'Uploading document {json_data["github_id"]}')
        insert_result = collection.insert_one(json_data)
        # Check if the insertion was successful
        if insert_result.inserted_id:
            print("Document inserted successfully with ID:", insert_result.inserted_id)
        else:
            print("Failed to insert document")

    # Close the MongoDB connection    
    client.close()

def check_parameters():
    # Check if a JSON file path is provided as a command-line argument
    if len(sys.argv) != 2:
        print("Usage: python script.py path/to/your/json/file.json")
        sys.exit(1)
    else:
        return sys.argv[1]





def main():
    for filename in os.listdir(data_path):
        if filename.endswith(".json"):
            file_path = os.path.join(data_path,filename)
            insert_json_file(file_path)
            print(f"Processed file: {filename}")
    
    


if __name__ == "__main__":
    main()