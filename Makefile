UID := $(shell id -u)
GID := $(shell id -g)
PROJECT_DIRECTORY := $(shell basename ${PWD})

define create-dirs
    @if [ ! -d "data" ]; then \
        echo "Creating data directory data"; \
        mkdir -p data; \
    fi
    @if [ ! -d "output" ]; then \
        echo "Creating output folder output"; \
        mkdir -p output; \
    fi
endef

define freeze_file
	git update-index --assume-unchanged $1
endef

define unfreeze_file
	git update-index --no-assume-unchanged $1
endef

check-dirs:
	$(create-dirs)


build: check-dirs
	docker-compose build --build-arg USER_ID=$(UID) --build-arg GROUP_ID=$(GID)

up: check-dirs
	docker-compose up -d

logs_etl:
	docker-compose logs -f otterdog_etl

logs_db:
	docker-compose logs -f otterdog_db

logs:
	docker-compose logs -f

build_otterod_db:
	docker-compose build otterdog_db

up_otterod_db:
	docker-compose up -d otterdog_db

shell_mongodb:
	docker exec -it otterdog_db /bin/bash

clean_compose:
	docker-compose stop
	docker-compose rm -f
	docker rmi -f otterdog_etl otterdog_db
	docker volume remove $(PROJECT_DIRECTORY)_mongodb-data
	docker volume remove $(PROJECT_DIRECTORY)_mongodb-log
	rm -rf output/*


freeze_etl_env:
	$(call freeze_file,./docker/otterdog_etl/env)

unfreeze_etl_env:
	$(call unfreeze_file,./docker/otterdog_etl/env)


update_project:
	git fetch
	git reset --keep origin/main

build_etl:
	docker-compose build --build-arg USER_ID=$(UID) --build-arg GROUP_ID=$(GID) otterdog_etl

run_etl:
	docker run -it --rm --name otterdog_etl -v ./scripts:/scripts -v ./data:/data -v ./output:/output otterdog_etl:latest

shell_etl:
	docker-compose run otterdog_etl /bin/bash


clean_etl:
	docker rm -f otterdog_etl
	docker rmi otterdog_etl

.PHONY: up clean_compose build_etl run_etl clean_etl shell_etl build check-dirs logs logs_db logs_etl
