#!/bin/bash


source /scripts/helpers.sh

echo "Downloading otterdog.json from https://github.com/EclipseFdn/otterdog-configs"
download_otterdog_configs

echo "Extracting JSONNET and Transforming into JSON"
extract_jsonnet_transform_json

echo "Loading data into MongoDB"
python /app/local/load.py


### By default, local MongoDB
if [ "$LOCAL_DB_ONLY" == "N" ]
then
    echo "Loading data into MongoDB Cloud"
    python /app/cloud/load.py
else
    echo "WARNING!!!: LOCAL_DB_ONLY IS NOT DEFINED OR WRONG VALUE FOR LOCAL_DB_ONLY=${LOCAL_DB_ONLY}"
fi