#!/bin/bash

check_dependencies() {
  local missing_dependencies=()

  # Check for jsonnet
  if ! command -v jsonnet &> /dev/null; then
    missing_dependencies+=("jsonnet")
  fi

  # Check for jb
  if ! command -v jb &> /dev/null; then
    missing_dependencies+=("jb")
  fi

  if [ ${#missing_dependencies[@]} -eq 0 ]; then
    echo "All required dependencies are installed."
  else
    echo "ERROR!!! The following dependencies are missing:"
    for dep in "${missing_dependencies[@]}"; do
      echo "  $dep"
    done
  fi

  # Checking directories
  if [ ! -d $DATA_PATH ]
  then
    mkdir -p ${DATA_PATH}
    echo "Created directory: ${DATA_PATH}"
  fi

  if [ ! -d ${OUTPUT_PATH} ]
  then
    mkdir -p ${OUTPUT_PATH}
    echo "Created directory: ${OUTPUT_PATH}"
  fi
}


