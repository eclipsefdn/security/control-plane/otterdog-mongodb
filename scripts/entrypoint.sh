#!/bin/bash


## Loop to execute every ${ETL_SLEEPTIME}

while true 
do
    echo "$(date "+%Y-%m-%d %H:%M:%S") ----------------- PROCESSING ETL, LOCAL_DB_ONLY=${LOCAL_DB_ONLY}?"
    ./etl.sh
    sleep ${ETL_SLEEPTIME}
done