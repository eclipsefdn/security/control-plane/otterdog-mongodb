export SCRIPT_PATH="$( cd -- "$(dirname ${BASH_SOURCE})" >/dev/null 2>&1 ; pwd -P )"
LIBRARY_PATH=${SCRIPT_PATH}/library
DATA_PATH="$(dirname ${SCRIPT_PATH})/data"
OUTPUT_PATH="$(dirname ${SCRIPT_PATH})/output"
# Sourcing global variables
source ${SCRIPT_PATH}/GLOBAL.sh

if [ -d "$LIBRARY_PATH" ]
then
    for script in "${LIBRARY_PATH}/"*.sh
    do
        if [ -f ${script} ]
        then
            source "${script}"
        fi
    done
else
    echo "ERROR!!!: Folder ${$LIBRARY_PATH} not found"
    exit 1
fi


check_dependencies
