#!/bin/bash

download_otterdog_configs(){
    curl --silent https://raw.githubusercontent.com/EclipseFdn/otterdog-configs/main/otterdog.json --output ${DATA_PATH}/otterdog.json
    echo "Downloaded: ${DATA_PATH}/otterdog.json"
}