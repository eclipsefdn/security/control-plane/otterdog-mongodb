#!/bin/bash


extract_jsonnet_transform_json(){
    local otterdog_file_path=${DATA_PATH}/otterdog.json
    ### TO-DO: In production subsitute [:2][] by []
    local arr_github_id=($(jq '.organizations[].github_id' ${otterdog_file_path} ))
    for g in ${arr_github_id[@]}
    do
        local gh_id=${g//\"/}
        echo "${gh_id} -- processing"

        ### Creating a temporary directory
        temp_dir=$(mktemp -d -t otterdog-${g//\"/}-XXXXXXXXXX)
        cd ${temp_dir}
        jb init
        jb install github.com/${gh_id}/.eclipsefdn/otterdog@main
        cp vendor/otterdog/${gh_id}.jsonnet vendor/otterdog-defaults/
        cd ${temp_dir}/vendor/otterdog-defaults/
        sed -i 's/vendor\/otterdog-defaults\///g' ${gh_id}.jsonnet
        jsonnet ${gh_id}.jsonnet > ${OUTPUT_PATH}/${gh_id}.json

        ### Cleaning up temporary directory
        rm -rf ${temp_dir}

    done
}