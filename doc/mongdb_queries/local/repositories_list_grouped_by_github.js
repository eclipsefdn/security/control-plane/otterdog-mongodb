db.organizations.aggregate([
    {
      $project: {
        github_id: 1,
        repositories: {
          $filter: {
            input: "$repositories",
            as: "repo",
            cond: {
              $eq: ["$$repo.branch_protection_rules", []]
            }
          }
        }
      }
    },
    {
      $addFields: {
        total_repositories: { $size: "$repositories" }
      }
    },
    {
      $match: {
        total_repositories: { $gt: 0 }
      }
    },
    {
      $project: {
        _id: 0,
        github_id: 1,
        total_repositories: 1,
        repositories_names: "$repositories.name"
      }
    },
    {
      $sort: {
        total_repositories: -1
      }
    }
  ])
  