db.organizations.aggregate([
    {
      $group: {
        _id: null,
        total_github_ids: { $sum: 1 },
        total_repositories: { $sum: { $size: "$repositories" } }
      }
    },
    {
      $project: {
        _id: 0,
        total_github_ids: 1,
        total_repositories: 1
      }
    }
  ])