db.getCollection("branch_security_rules").aggregate([{
    $project:{
        _id:0,
        github_id: 1,
      repositories: {
          $map:{
              input: "$repositories",
              as: "repo",
              in: {
                  repo_name: "$$repo.name",
                  repo_default_branch: "$$repo.default_branch",
                  branch_protection_rules: "$$repo.branch_protection_rules",
              }
          }
      },
    }
}, {
    $unwind: "$repositories"
},{
    $project:{
        _id:0,
        github_id: 1,
        repo_name: "$repositories.repo_name",
        repo_default_branch: "$repositories.repo_default_branch",
        repo_branch_protection_rules: "$repositories.branch_protection_rules",

    }
}, {
    $sort: {
        total_repo_branch_protection_rules: -1
    }
}, {
    $project: {
        _id:0,
        github_id: 1,
        repo_name: 1,
        repo_default_branch: 1,
        repo_branch_protection_rules: {
            $filter: {
                input: "$repo_branch_protection_rules",
                as: "bpr",
                cond: { $eq: ["$$bpr.pattern", "$repo_default_branch"] }
            }
        }
    }
},{
    $project: {
        _id:0,
        github_id: 1,
        repo_name: 1,
        repo_default_branch: 1,
        repo_default_branch_protection: {
            $cond: {
                if: { $eq: [ "$repo_branch_protection_rules", []]},
                then: "not protected",
                else: "protected"
            }
        }
    }
}
])