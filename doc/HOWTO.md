# HOW-TO 

## How to get the total of github_id and repositories

* Local instance
[mongdb_queries/local/total_of_github_id_and_repositories.js](./mongdb_queries/local/total_of_github_id_and_repositories.js)

* Free MongoDB cloud instance
[mongdb_queries/cloud/tota_of_github_id_and_repositories.js](./mongdb_queries/cloud/tota_of_github_id_and_repositories.js)

## How to get a list of repositories name grouped by github with total of repositories where branch_protection_rules is empty

* Local instance
[mongdb_queries/local/repositories_list_grouped_by_github.js](./mongdb_queries/local/repositories_list_grouped_by_github.js)

* Free MongoDB cloud instance
[mongdb_queries/cloud/repositories_list_grouped_by_github.js](./mongdb_queries/cloud/repositories_list_grouped_by_github.js)

## How to get a list of documents with default_branch (name) and default_branch_protection

* Local instance
[mongdb_queries/local/documents_list_with_default_branch_and_branch_protection.js](./mongdb_queries/local/documents_list_with_default_branch_and_branch_protection.js)

* Free MongoDB cloud instance

[mongdb_queries/cloud/documents_list_with_default_branch_and_branch_protection.js](./mongdb_queries/cloud/documents_list_with_default_branch_and_branch_protection.js)
