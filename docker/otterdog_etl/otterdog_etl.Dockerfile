FROM gcc:9.5.0-bullseye as build_jsonnet
WORKDIR /app
RUN git clone https://github.com/google/jsonnet.git /app
RUN make

FROM golang:latest as build_jsonnet_bundler
RUN go install -a github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb@latest

FROM python:3.8-slim

# Set the user ID and group ID to match the host user's IDs
ARG USER_ID
ARG GROUP_ID
ARG MONGODB_VERSION=7.0
ARG MONGOSH_VERSION=1.10.5
ARG ETL_SLEEPTIME=1800
ARG LOCAL_DB_ONLY


COPY --from=build_jsonnet /app/jsonnet /bin
COPY --from=build_jsonnet /app/jsonnetfmt /bin
COPY --from=build_jsonnet_bundler /go/bin/jb /bin

RUN apt update && apt install -y --no-install-recommends curl jq git iputils-ping wget gnupg coreutils openssl
RUN set -x \
    && curl -fsSL https://www.mongodb.org/static/pgp/server-${MONGODB_VERSION}.asc | gpg --dearmor -o /etc/apt/trusted.gpg.d/mongodb-org-${MONGODB_VERSION}.gpg \
    && echo "deb https://repo.mongodb.org/apt/debian bullseye/mongodb-org/${MONGODB_VERSION} main" > /etc/apt/sources.list.d/mongodb-org-${MONGODB_VERSION}.list

RUN set -x && apt-get update && apt-get --yes --no-install-recommends install \
    mongodb-mongosh=${MONGOSH_VERSION}

# Create a user and group in the container with matching IDs
RUN groupadd -g $GROUP_ID hostgroup && \
    useradd -u $USER_ID -g $GROUP_ID -ms /bin/bash hostuser

USER hostuser
WORKDIR /scripts

COPY requirements.txt /app/requirements.txt

RUN pip install -r /app/requirements.txt

CMD ["/scripts/entrypoint.sh"]
