db = db.getSiblingDB('otterdogdb')

db.createUser(
    {
        user: "otterdog",
        pwd: "otterdog",
        roles: [
            {
                role: "readWrite",
                db: "otterdogdb"
            }
        ]
    }
);

db.createCollection("organizations")

db.organizations.insertOne({_id:1, name: "test"})
db.organizations.deleteOne({name: "test"})
