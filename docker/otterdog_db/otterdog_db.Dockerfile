FROM mongo:latest

RUN apt update && apt install -y iputils-ping

# Copy initialization script
COPY mongo-init.js /docker-entrypoint-initdb.d/

EXPOSE 27017
CMD ["mongod"]
