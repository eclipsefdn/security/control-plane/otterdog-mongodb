# otterdog-mongodb

This is a POC to build a MongoDB database to store otterdog data, hosted locally and in free MongoDB instance. However this project use a free cloud version of MongoDB. Please reach out to security team to get more information about it. 


## Arhictecure

The database is based on data got from the jsonnet applied to those Github Eclipse Organization where otterdog have been installed

This project create the containers below:
* otterdog_etl: A container with scripts to extract, transforms and load each jsonet
* otterdog_db: A container with scripts to MongoDB already installed

The data can be queried by using:
* [Studio 3T](https://studio3t.com/free/)
* [MongoDB Shell](https://www.mongodb.com/try/download/shell)
* [MongoDB Compass (GUI)](https://www.mongodb.com/try/download/compass)


## Deploying containers
* After cloning repositorie execute the snippet below
```bash
make build
make up
```
## Deploying containers and uploading data to the free MongdoDB cloud instance
```bash
export export OTTERDOG_LOCAL_DB_ONLY=N
make build
make up
```
### Caveats
Please follow the steps below to upload data to the free MongoDB cloud instance and reach out to the security team for assistance:
* Requesting access to cloud credentials and configure your local environment file [docker/otterdog_etl/env](./docker/otterdog_etl/env)
* Providing your Internet IP


## HOW-TO
Please go to docment [HOWTO.md](./doc/HOWTO.md)